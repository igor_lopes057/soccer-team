from constants import CHOICE_OPTIONS, YES_OR_NO_OPTIONS

def menu(data):
    print('\nOpções:')
    print('1. Inclusão de dado.')
    print('2. Alteração de dado.')
    print('3. Deleção de dado.')
    print('4. Consulta de dados.')
    print('5. Sair do sistema.')
    choice = int(input('\nEscolha uma opção: '))
    no_data_found = no_data(data)
    if choice == CHOICE_OPTIONS[0]:
        insert_data(data)
        menu(data)
    elif choice == CHOICE_OPTIONS[1] and not no_data_found:
        modify_data(data)
    elif choice == CHOICE_OPTIONS[2] and not no_data_found:
        delete_data(data)
    elif choice == CHOICE_OPTIONS[3]:
        show_data(data)
        menu(data)
    elif choice == CHOICE_OPTIONS[4]:
        exit()
    else:
        not_valid_choice(data)


def not_valid_choice(data):
    print('\nTente novamente, escolha não é válida!')
    menu(data)


def no_data(data):
    return len(data) == 0


def insert_data(data):
    required_data = {}
    try:
        required_data['id'] = int(input('Digite o código do jogador: '))
        required_data['name'] = str(input('Digite o nome do jogador: '))
        required_data['position'] = str(input('Digite a posição do jogador: '))
        required_data['height'] = float(input('Digite a altura do jogador: '))
        required_data['weight'] = float(input('Digite o peso do jogador: '))
        required_data['leg_preference'] = str(
            input('Digite se o jogador é destro ou canhoto: '))
        required_data['address'] = str(input('Digite o endereço do jogador: '))
        print('\nInserido!')
    except ValueError:
        insert_data(data)
    finally:
        return data.append(required_data)


def start_adding_data(data):
    choice = str(input('Deseja adicionar um dado para começar? (S/N) '))
    if choice.upper() == YES_OR_NO_OPTIONS[0]:
        insert_data(data)
        menu(data)
    if choice.upper() == YES_OR_NO_OPTIONS[1]:
        exit()
    if choice.upper() not in YES_OR_NO_OPTIONS:
        start_adding_data(data)


def show_data(data):
    no_data_found = no_data(data)
    if no_data_found:
        print('Sem dados!')
    else:
        print('\nDados existentes\n')
        for i in data:
            print(f'Posição {data.index(i) + 1}: {i}')


def delete_data(data):
    show_data(data)
    choice = int(
        input('Digite a posição que deseja deletar o dado: '))
    try:
        del data[choice - 1]
        print('Data atualizada!\n')
        show_data(data)
    except IndexError:
        print('Digite uma posição válida!')
        delete_data(data)
    finally:
        menu(data)


def modify_data(data):
    show_data(data)
    choice = int(
        input('Digite a posição que deseja alterar o dado: '))
    try:
        del data[choice - 1]
        print('\nInsira agora novamente o dado corretamente...\n')
        insert_data(data)
        show_data(data)
    except IndexError:
        print('Digite uma posição válida!')
        modify_data(data)
    finally:
        menu(data)


def exit():
    print('Saindo do sistema...\n')
    print('Obrigado por utilizar!')
    print('-'*100)


def start_program():
    print('-'*100)
    print('Seja bem vindo!\n')
    data = []
    no_data_found = no_data(data)
    if no_data_found:
        print('Sem dados adicionados!')
        start_adding_data(data)
    else:
        show_data(data)
        menu(data)
